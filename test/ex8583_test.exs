defmodule Ex8583Test do
  use ExUnit.Case
  alias Ex8583.Message
  alias Ex8583.FieldIds
  alias Ex8583.Marshal.Xml
  alias Ex8583.Marshal.Ascii

  test "xml marshal / unmarshal" do
    xml_example = """
    <isomsg>
    <field id="0" value="0200" />
    <field id="43" value="ZIB Head Office ATM V/I Lagos 01NG" />
    <field id="64" value="0102030405060708" type="binary" />
    <isomsg id="127">
    <field id="2" value="0000387020" />
    <field id="12" value="ZIBeTranzSnk" />
    </isomsg>
    </isomsg>
    """
    xml = Message.new
    |> Message.set_mti("0200")
    |> Message.set(FieldIds.card_acceptor_name_location, "ZIB Head Office ATM V/I Lagos 01NG")
    |> Message.set(FieldIds.message_authentication_code, <<1,2,3,4,5,6,7,8>>)
    |> Message.set([127,2],"0000387020")
    |> Message.set([127,12],"ZIBeTranzSnk")
    |> Xml.marshal

    assert xml_example == xml
    assert ~c"0200" == Message.get(Xml.unmarshal(xml), 0)
  end

  test "ascii marshal / unmarshal" do 
    ascii_example = "08002000010000C0000230000004511111111222222222222222022This is a Test Message"

    ascii = Message.new
    |> Message.set_mti("0800")
    |> Message.set(3,  "300000")
    |> Message.set(24, "045")
    |> Message.set(41, "11111111")
    |> Message.set(42, "222222222222222")
    |> Message.set(63, "This is a Test Message")
    |> Ascii.marshal

    assert ascii_example == ascii
    assert ~c"0800" == Message.get(Ascii.unmarshal(ascii), 0)
  end

#  test "ascii marshal / unmarshal 2" do 
#    marshalling_options = [{:field_marshaller, :erl8583_marshaller_ascii},
#						               {:bitmap_marshaller, :erl8583_marshaller_ascii},
#						               {:mti_marshaller, :erl8583_marshaller_ascii},
#						               {:end_marshaller, :erl8583_marshaller_ascii}]
#
#    ascii = '00426000030000 02007024058000C00000164893530003260112000000000000100000000001161101200003083030303832393335303230303938343234303030303030'
#    #ascii = ''
#    #hex_ascii = :erl8583_convert.ascii_hex_to_binary_list(ascii)
#    #IO.inspect hex_ascii 
#    #:erl8583_marshaller_ebcdic.unmarshal(ascii)
#    #:erl8583_marshaller.unmarshal(ascii, marshalling_options)
#    
#
#    #    IO.inspect 
#    #7024058000C00000
#    #0111000000100100000001011000000000000000110000000000000000000000
#    #2 3 4 11 14 22 24 25 41 42
#    b = '02003020058020C800040000000000000010000017730022000100375021070222424530D13025011364259900000F30303030323230393032303030323331383030303030306642353032313037303232323432343533305E5441524A4554412020424E2D5052455041474F202020202020205E313330323530313133363432353939303030303030000630303030'
#    b = '02007024058000C0000016489353000326011200000000000010000000000116110120000308303030383239333530323'
#    #'0200 7024058000C00000 16_4893530003260112 000000 000000100000 000001 1611 012 000 03 08303030 383239333530323 0303938343234303030303030'
#    # MTI  BITMAP                2                3       4          11     14   22  24 25   41           42
#    
#    #|> to_string
#    #|> Base.decode16
#    #|> elem(1)
#    #|> :erlang.binary_to_list
#    #|> IO.inspect
#    message = :erl8583_marshaller.unmarshal(b, marshalling_options)
#    #IO.inspect message
#
##    IO.puts :erl8583_message.get(2, message)
##    IO.puts :erl8583_message.get(3, message)
##    IO.puts :erl8583_message.get(4, message)
##    IO.puts :erl8583_message.get(11, message)
##    IO.puts :erl8583_message.get(14, message)
##    IO.puts :erl8583_message.get(22, message)
##    IO.puts :erl8583_message.get(24, message)
##    IO.puts :erl8583_message.get(25, message)
##    IO.puts :erl8583_message.get(41, message)
##    IO.puts :erl8583_message.get(42, message)
#
#
#    b = '0210203801000E800000000000000001001325030300033630363330303133323532333833343731333030303030383239333500000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'
#    b = '0210203801000A8000020000000000010519400303000336303633303531393430303235313030303832393335004200403232464F4E444F5320494E535546494349454E54455320464F4E444F5320494E535546494349454E0000000000000000000000000000000000000000000000000000000000000000'
#
#    message = :erl8583_marshaller.unmarshal(b, marshalling_options)
#
#
#
#
#    #IO.inspect Ascii.unmarshal(ascii)
#
##    :erl8583_convert.ascii_hex_to_binary_list(ascii)
##    |> to_string
##    |> IO.inspect
##
##    ascii = Message.new
##    |> Message.set_mti("0200")
##    |> Message.set(3,  "300000")
##    |> Message.set(24, "045")
##    |> Message.set(41, "11111111")
##    |> Message.set(42, "222222222222222")
##    |> Message.set(63, "This is a Test Message")
##    #|> Ascii.marshal
##    #|> :erl8583_marshaller_binary.marshal
##    #|> :erl8583_convert.binary_list_to_ascii_hex
##    #|> :erl8583_convert.ascii_hex_to_digit
##    |> :erl8583_marshaller.marshal(marshalling_options)
##
#
#
#  end
end
