defmodule Ex8583.Mixfile do
  use Mix.Project

  def project do
    [app: :ex8583,
     version: "1.0.0",
     elixir: "~> 1.14",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps()]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [
      extra_applications: [:xmerl, :logger, :runtime_tools]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type `mix help deps` for more examples and options
  defp deps do
    [
      {:erl8583, git: "https://github.com/josepablocastro/erl8583.git", ref: "cfeee77" },
      {:quaff, "~> 1.0"}
    ]
  end
end
