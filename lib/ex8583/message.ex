defmodule Ex8583.Message do
  def new, do: :erl8583_message.new()

  def set(message, field_id, value) when is_binary(value) do
    fixed_value =
      case String.printable?(value) do
        true -> to_charlist(value)
        false -> value
      end

    :erl8583_message.set(field_id, fixed_value, message)
  end

  def set(message, field_id, value) do
    :erl8583_message.set(field_id, value, message)
  end

  def set_mti(message, value),
    do: :erl8583_message.set_mti(to_charlist(value), message)

  def get(message, field_id),
    do: :erl8583_message.get(field_id, message)

  def get_fields(message),
    do: :erl8583_message.get_fields(message)
end
