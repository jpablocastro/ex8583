defmodule Ex8583.Marshal.Xml do 
  def marshal(message) do 
    message
    |> :erl8583_marshaller_xml.marshal()
    |> to_string()
  end

  def unmarshal(message) do 
    message
    |> to_charlist()
    |> :erl8583_marshaller_xml.unmarshal()
  end
end


