defmodule Ex8583.Marshal.Ascii do 
  def marshal(message) do 
    message
    |> :erl8583_marshaller_ascii.marshal()
    |> to_string()
  end

  def unmarshal(message) do 
    message
    |> to_charlist()
    |> :erl8583_marshaller_ascii.unmarshal()
  end
end


