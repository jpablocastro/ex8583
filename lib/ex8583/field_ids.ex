defmodule Ex8583.FieldIds do
  require Quaff
  Quaff.include_lib("erl8583/include/erl8583_field_ids.hrl")

  ## Defines identifiers for ISO 8583 fields.
  def mti,                                 do: @_MTI
  def bitmap_extended,                     do: @_BITMAP_EXTENDED
  def pan,                                 do: @_PAN
  def proc_code,                           do: @_PROC_CODE
  def amount_tran,                         do: @_AMOUNT_TRAN
  def amount_settle,                       do: @_AMOUNT_SETTLE
  def amount_cardholder_billing,           do: @_AMOUNT_CARDHOLDER_BILLING
  def transmission_date_time,              do: @_TRANSMISSION_DATE_TIME
  def amount_cardholder_billing_fee,       do: @_AMOUNT_CARDHOLDER_BILLING_FEE
  def conversion_rate_settle,              do: @_CONVERSION_RATE_SETTLE
  def conversion_rate_cardholder_billing,  do: @_CONVERSION_RATE_CARDHOLDER_BILLING
  def systems_trace_audit_number,          do: @_SYSTEMS_TRACE_AUDIT_NUMBER

  # 1987 declaration for field 12
  def time_local_tran,                     do: @_TIME_LOCAL_TRAN
  # 1993 declaration for field 12
  def date_and_time_local_tran,            do: @_DATE_AND_TIME_LOCAL_TRAN

  # 1987 declaration for field 12
  def date_local_tran,                     do: @_DATE_LOCAL_TRAN
  # 1993 declaration for field 13
  def date_eff,                            do: @_DATE_EFF

  def date_exp,                            do: @_DATE_EXP
  def date_settle,                         do: @_DATE_SETTLE
  def date_conversion,                     do: @_DATE_CONVERSION
  def date_capture,                        do: @_DATE_CAPTURE
  def merchant_type,                       do: @_MERCHANT_TYPE
  def acquirer_country_code,               do: @_ACQUIRER_COUNTRY_CODE
  def pan_ext_country_code,                do: @_PAN_EXT_COUNTRY_CODE
  def forwarding_inst_country_code,        do: @_FORWARDING_INST_COUNTRY_CODE
  
  # 1987 declaration for field 22
  def pos_entry_mode,                      do: @_POS_ENTRY_MODE
  # 1993 declaration for field 22
  def pos_data_code,                       do: @_POS_DATA_CODE
  
  def card_sequence_number,                do: @_CARD_SEQUENCE_NUMBER
  
  # 1987 declaration for field 24
  def network_international_id,            do: @_NETWORK_INTERNATIONAL_ID
  # 1993 declaration for field 24
  def function_code,                       do: @_FUNCTION_CODE
  
  # 1987 declaration for field 25
  def pos_condition_code,                  do: @_POS_CONDITION_CODE
  # 1993 declaration for field 25
  def message_reason_code,                 do: @_MESSAGE_REASON_CODE
  
  # 1987 declaration for field 26
  def pos_capture_code,                    do: @_POS_CAPTURE_CODE
  # 1993 declaration for field 26
  def card_acceptor_business_code,         do: @_CARD_ACCEPTOR_BUSINESS_CODE
  
  def authorizing_id_resp_len,             do: @_AUTHORIZING_ID_RESP_LEN
  def amount_tran_fee,                     do: @_AMOUNT_TRAN_FEE
  def amount_settle_fee,                   do: @_AMOUNT_SETTLE_FEE
  
  # 1987 declaration for field 30
  def amount_tran_processing_fee,          do: @_AMOUNT_TRAN_PROCESSING_FEE
  # 1993 declaration for field 30
  def amount_original,                     do: @_AMOUNT_ORIGINAL
  
  # 1987 declaration for field 31
  def amount_settle_processing_fee,        do: @_AMOUNT_SETTLE_PROCESSING_FEE
  # 1993 declaration for field 31
  def acquirer_reference_data,             do: @_ACQUIRER_REFERENCE_DATA
  
  def acquiring_inst_id_code,              do: @_ACQUIRING_INST_ID_CODE
  def forwarding_inst_id_code,             do: @_FORWARDING_INST_ID_CODE
  def pan_extended,                        do: @_PAN_EXTENDED
  def track_2_data,                        do: @_TRACK_2_DATA
  def track_3_data,                        do: @_TRACK_3_DATA
  def retrieval_ref_num,                   do: @_RETRIEVAL_REF_NUM
  def authorization_id_resp,               do: @_AUTHORIZATION_ID_RESP
  def resp_code,                           do: @_RESP_CODE
  def service_restriction_code,            do: @_SERVICE_RESTRICTION_CODE
  def card_acceptor_terminal_id,           do: @_CARD_ACCEPTOR_TERMINAL_ID
  def card_acceptor_id_code,               do: @_CARD_ACCEPTOR_ID_CODE
  def card_acceptor_name_location,         do: @_CARD_ACCEPTOR_NAME_LOCATION
  def additional_resp_data,                do: @_ADDITIONAL_RESP_DATA
  def track_1_data,                        do: @_TRACK_1_DATA
  def additional_data_iso,                 do: @_ADDITIONAL_DATA_ISO
  def additional_data_national,            do: @_ADDITIONAL_DATA_NATIONAL
  def additional_data_private,             do: @_ADDITIONAL_DATA_PRIVATE
  def currency_code_tran,                  do: @_CURRENCY_CODE_TRAN
  def currency_code_settle,                do: @_CURRENCY_CODE_SETTLE
  def currency_code_cardholder_billing,    do: @_CURRENCY_CODE_CARDHOLDER_BILLING
  def personal_id_number_data,             do: @_PERSONAL_ID_NUMBER_DATA
  def security_related_control_info,       do: @_SECURITY_RELATED_CONTROL_INFO
  def additional_amounts,                  do: @_ADDITIONAL_AMOUNTS
  
  # 1987 definition for field 55
  def reserved_iso1,                       do: @_RESERVED_ISO1
  # 1993 definition for field 55
  def icc_system_related_data,             do: @_ICC_SYSTEM_RELATED_DATA
  
  # 1987 definition for field 56
  def reserved_iso2,                       do: @_RESERVED_ISO2
  # 1993 definition for field 56
  def original_data_elements_1993,         do: @_ORIGINAL_DATA_ELEMENTS_1993
  
  def reserved_national1,                  do: @_RESERVED_NATIONAL1
  def reserved_national2,                  do: @_RESERVED_NATIONAL2
  def reserved_national3,                  do: @_RESERVED_NATIONAL3
  def reserved_private1,                   do: @_RESERVED_PRIVATE1
  def reserved_private2,                   do: @_RESERVED_PRIVATE2
  def reserved_private3,                   do: @_RESERVED_PRIVATE3
  def reserved_private4,                   do: @_RESERVED_PRIVATE4
  def message_authentication_code,         do: @_MESSAGE_AUTHENTICATION_CODE
  def settle_code,                         do: @_SETTLE_CODE
  def extended_payment_code,               do: @_EXTENDED_PAYMENT_CODE
  def receiving_institution_country_code,  do: @_RECEIVING_INSTITUTION_COUNTRY_CODE
  def settle_institution_country_code,     do: @_SETTLE_INSTITUTION_COUNTRY_CODE
  def network_management_information_code, do: @_NETWORK_MANAGEMENT_INFORMATION_CODE
  def message_number,                      do: @_MESSAGE_NUMBER
  def message_number_last,                 do: @_MESSAGE_NUMBER_LAST
  def date_action,                         do: @_DATE_ACTION
  def credits_number,                      do: @_CREDITS_NUMBER
  def credits_reversal_number,             do: @_CREDITS_REVERSAL_NUMBER
  def debits_number,                       do: @_DEBITS_NUMBER
  def debits_reversal_number,              do: @_DEBITS_REVERSAL_NUMBER
  def transfer_number,                     do: @_TRANSFER_NUMBER
  def transfer_number_reversal,            do: @_TRANSFER_NUMBER_REVERSAL
  def inquiries_number,                    do: @_INQUIRIES_NUMBER
  def authorizations_number,               do: @_AUTHORIZATIONS_NUMBER
  def credits_processing_fee_amount,       do: @_CREDITS_PROCESSING_FEE_AMOUNT
  def credits_transaction_fee_amount,      do: @_CREDITS_TRANSACTION_FEE_AMOUNT
  def debits_processing_fee_amount,        do: @_DEBITS_PROCESSING_FEE_AMOUNT
  def debits_transaction_fee_amount,       do: @_DEBITS_TRANSACTION_FEE_AMOUNT
  def credits_amount,                      do: @_CREDITS_AMOUNT
  def credits_reversal_amount,             do: @_CREDITS_REVERSAL_AMOUNT
  def debits_amount,                       do: @_DEBITS_AMOUNT
  def debits_reversal_amount,              do: @_DEBITS_REVERSAL_AMOUNT
  def original_data_elements,              do: @_ORIGINAL_DATA_ELEMENTS
  def file_update_code,                    do: @_FILE_UPDATE_CODE
  def file_security_code,                  do: @_FILE_SECURITY_CODE
  def resp_indicator,                      do: @_RESP_INDICATOR
  def service_indicator,                   do: @_SERVICE_INDICATOR
  def replacement_amounts,                 do: @_REPLACEMENT_AMOUNTS
  def message_security_code,               do: @_MESSAGE_SECURITY_CODE
  def amount_net_settle,                   do: @_AMOUNT_NET_SETTLE
  def payee,                               do: @_PAYEE
  def settle_institution_id_code,          do: @_SETTLE_INSTITUTION_ID_CODE
  def receiving_institution_id_code,       do: @_RECEIVING_INSTITUTION_ID_CODE
  def file_name,                           do: @_FILE_NAME
  def account_id1,                         do: @_ACCOUNT_ID1
  def account_id2,                         do: @_ACCOUNT_ID2
  def tran_description,                    do: @_TRAN_DESCRIPTION
  def message_authentication_code2,        do: @_MESSAGE_AUTHENTICATION_CODE2
  
end
